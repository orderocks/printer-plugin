function OrderoxBtPrinterPlugin() {}

// The function that passes work along to native shells
// Message is a string, duration may be 'long' or 'short'
OrderoxBtPrinterPlugin.prototype.printText = function(message, printerName, successCallback, errorCallback) {
  var options = {};
  options.message = message;
  options.printer_name = printerName;
  cordova.exec(successCallback, errorCallback, 'OrderoxBtPrinterPlugin', 'printText', [options]);
}
OrderoxBtPrinterPlugin.prototype.checkPrinterStatus = function(printerName, successCallback, errorCallback) {
  var options = {};
  options.message = '';
  options.printer_name = printerName;
  cordova.exec(successCallback, errorCallback, 'OrderoxBtPrinterPlugin', 'checkPrinterStatus', [options]);
}

// Installation constructor that binds OrderoxBtPrinterPlugin to window
OrderoxBtPrinterPlugin.install = function() {
  if (!window.plugins) {
    window.plugins = {};
  }
  window.plugins.OrderoxBtPrinterPlugin = new OrderoxBtPrinterPlugin();
  return window.plugins.OrderoxBtPrinterPlugin;
};
cordova.addConstructor(OrderoxBtPrinterPlugin.install)