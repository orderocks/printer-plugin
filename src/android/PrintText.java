package sk.orderox.cordova.plugin;


import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.util.Log;

public class PrintText {

    private static BluetoothSocket socket;
    private static OutputStream outputStream;
    private static InputStream inputStream;
    private static volatile boolean stopWorker;

    private static byte[] readBuffer;
    private static int readBufferPosition;
    public static boolean connected = false;

    public static void printText(String txtvalue, String printerName) throws Exception {
        OrderoxBtPrinterPlugin.debug("printText " + txtvalue + ", printer: " + printerName);
        byte[] buffer = txtvalue.getBytes();
        byte[] PrintHeader = {(byte) 0xAA, 0x55, 2, 0};
        PrintHeader[3] = (byte) buffer.length;
        InitPrinter(printerName);
        try {
            OrderoxBtPrinterPlugin.debug("outputStream.write " + txtvalue);
            outputStream.write(txtvalue.getBytes());
        } catch (Exception e) {
            OrderoxBtPrinterPlugin.debug("printText exception " + e.toString());
            connected = false;
            throw new Exception("p1-disconnected");
        }
        outputStream.close();
        socket.close();
    }

    private static void InitPrinter(String printerName) throws Exception {
        OrderoxBtPrinterPlugin.debug("InitPrinter " + printerName);
//        if (!connected) {
            CheckPrinterStatus check = new CheckPrinterStatus();
            socket = check.getPrinter(printerName);
            connected = true;
//        }
        if (!socket.isConnected()) {
            throw new Exception("p2-socket-disconnected");
        }
        outputStream = socket.getOutputStream();
        inputStream = socket.getInputStream();
    }
}
