package sk.orderox.cordova.plugin;
// Cordova-required packages
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.content.Intent;
import android.content.Context;
import android.util.Log;
import android.bluetooth.BluetoothAdapter;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class OrderoxBtPrinterPlugin extends CordovaPlugin {

    public static String logs = "";
    public static String logGroup = "";

    public static void debug(String text) {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        if(OrderoxBtPrinterPlugin.logGroup.equals("")) {
            OrderoxBtPrinterPlugin.logGroup = new SimpleDateFormat("yyyyMMdd_HHmmssSSS").format(Calendar.getInstance().getTime());
        }
        String debugtext = "G:"+OrderoxBtPrinterPlugin.logGroup+"|D:"+timeStamp+"|T:"+text+"|||";
        Log.i("OXPRINT", debugtext);
        OrderoxBtPrinterPlugin.logs += debugtext;
    }

  @Override
  public boolean execute(String action, JSONArray args,
    final CallbackContext callbackContext) {
      String message;
      String printerName;
      OrderoxBtPrinterPlugin.debug("OrderoxBtPrinterPlugin execute "+action);
      try {
        JSONObject options = args.getJSONObject(0);
        message = options.getString("message");
        printerName = options.getString("printer_name");
          OrderoxBtPrinterPlugin.debug("OrderoxBtPrinterPlugin message "+message+", printerName: "+printerName);
      } catch (JSONException e) {
        callbackContext.error("Error encountered: " + e.getMessage());
        OrderoxBtPrinterPlugin.logs = "";
        return false;
      }
      if (printerName.equals("")) {
          callbackContext.error("o1-Empty printer name");
          OrderoxBtPrinterPlugin.logs = "";
          return false;
      }
      BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
      if (!bluetoothAdapter.isEnabled()) {
          callbackContext.error("o4-Bluetooth not enabled");
          OrderoxBtPrinterPlugin.logs = "";
          return false;
      }
      if(action.equals("printText")) {
          try {
              OrderoxBtPrinterPlugin.debug("cordova.getActivity().getApplicationContext");
              Context context = cordova.getActivity().getApplicationContext();
              OrderoxBtPrinterPlugin.debug("new intent");
              Intent intent = new Intent(context, SendService.class);
              intent.putExtra("text", message);
              intent.putExtra("printerName", printerName);
              OrderoxBtPrinterPlugin.debug("cordova.getActivity().startActivity(intent)");
              cordova.getActivity().startActivity(intent);
//              PrintText.printText(message, printerName);
              callbackContext.success(OrderoxBtPrinterPlugin.logs);
              OrderoxBtPrinterPlugin.logs = "";
//              PluginResult pluginResult = new PluginResult(PluginResult.Status.OK);
//              callbackContext.sendPluginResult(pluginResult);

              return true;
          } catch (Exception e) {
              String err = e.getMessage()+", PLUGINLOGS:"+OrderoxBtPrinterPlugin.logs;
              callbackContext.error(err);
          }
          OrderoxBtPrinterPlugin.logs = "";
          return false;
      }
      if(action.equals("checkPrinterStatus")) {
          try {
              PrintText.printText("", printerName);
              callbackContext.success(OrderoxBtPrinterPlugin.logs);
//              PluginResult pluginResult = new PluginResult(PluginResult.Status.OK);
//              callbackContext.sendPluginResult(pluginResult);

              return true;
          } catch (Exception e) {
              String err = e.getMessage()+", PLUGINLOGS:"+OrderoxBtPrinterPlugin.logs;
              callbackContext.error(err);
          }
          OrderoxBtPrinterPlugin.logs = "";
          return false;
      }
      callbackContext.error("Unknown method: "+action);
      OrderoxBtPrinterPlugin.logs = "";
      return false;
  }

}