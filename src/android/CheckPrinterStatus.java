package sk.orderox.cordova.plugin;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import java.lang.reflect.Method;
import java.util.Set;
import android.util.Log;

public class CheckPrinterStatus {
    public BluetoothSocket getPrinter(String printerName) throws Exception {
        OrderoxBtPrinterPlugin.debug("CheckPrinterStatus printer: "+printerName);
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!bluetoothAdapter.isEnabled()) {
            throw new Exception("c4-Bluetooth not enabled");
        }
        Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                if (device.getName().equals(printerName)) //Note, you will need to change this to match the name of your device
                {
                    bluetoothAdapter.cancelDiscovery();
                    Method m = device.getClass().getMethod("createRfcommSocket", new Class[]{int.class});
                    BluetoothSocket socket = (BluetoothSocket) m.invoke(device, 1);
                    try {
                        if(!socket.isConnected()) {
                            OrderoxBtPrinterPlugin.debug("CheckPrinterStatus try connect");
                            socket.connect();
                        } else {
                            OrderoxBtPrinterPlugin.debug("CheckPrinterStatus socket already connected");
                        }
                    } catch (Exception e) {
                        OrderoxBtPrinterPlugin.debug("socket.connect() Exception: "+e.toString());
                        throw new Exception("c1-Unable to connect to printer");
                    }
                    OrderoxBtPrinterPlugin.debug("CheckPrinterStatus connected");
                    return socket;
                }
            }
            throw new Exception("c2-Printer not found");
        }
        throw new Exception("c3-No BT Devices found");
    }
}