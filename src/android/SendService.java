package sk.orderox.cordova.plugin;

import android.app.IntentService;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Set;

public class SendService extends IntentService {

    private static boolean stopWorker;
    private int connectTries = 0;
    private InputStream inputStream;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public SendService() {
        super("BTOXSendService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
//        BluetoothDevice device = (BluetoothDevice) intent.getParcelableExtra("device");
        BluetoothDevice device = null;
        try {
            device = getDevice(intent.getStringExtra("printerName"));
        } catch (Exception e) {
            Log.i("BTTEST", "getDevice exception: "+e.getMessage());
            e.printStackTrace();
            return;
        }
        String text = intent.getStringExtra("text");
//        boolean uuid = device.fetchUuidsWithSdp();
//        try {
//            Thread.sleep(5000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        ParcelUuid[] uuids = device.getUuids();
        Log.i("BTTEST", "Printing text: "+text);
        connectTries = 0;
        BluetoothSocket socket = null;
        try {
            socket = getSocket(device);
        } catch (InvocationTargetException | IllegalAccessException | InterruptedException | IOException | NoSuchMethodException e) {
            Log.i("BTTEST", "getSocket exception: "+e.getMessage());
            e.printStackTrace();
            return;
        }
        try {
            inputStream = socket.getInputStream();
            OutputStream outputStream = socket.getOutputStream();
//            ListenerThread listenerThread = new ListenerThread((InputStream) inputStream);
//            listenerThread.start();
            beginListenForData();
            Thread.sleep(1000);
            outputStream.write(text.getBytes("UTF-8"));

            Log.i("BTTEST", "BEFORE CLOSE");
//            outputStream.flush();
            outputStream.close();
            Log.i("BTTEST", "outputStream.close");
            inputStream.close();
            Log.i("BTTEST", "inputStream.close");
            socket.close();
            Log.i("BTTEST", "socket.close");
//            listenerThread.stopWorker();
            stopWorker = true;

        } catch (Exception e) {
            Log.i("BTTEST", "WRITE exception: "+e.getMessage());
            e.printStackTrace();
        }
    }

    public static BluetoothDevice getDevice(String printerName) throws Exception {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!bluetoothAdapter.isEnabled()) {
            throw new Exception("4-Bluetooth not enabled");
        }
        Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();
        Log.i("BTTEST", "pairedDevices done ");
        if (pairedDevices.size() > 0) {
//            MainActivity.debug("pairedDevices done");

            Log.i("BTTEST", "pairedDevices size > 0");
//            MainActivity.debug("pairedDevices size > 0");
            for (BluetoothDevice device : pairedDevices) {
                if (device.getName().equals(printerName)) {
                    bluetoothAdapter.cancelDiscovery();

                    return device;
                }
            }
        }
        throw new Exception("3-No BT Devices found");

    }

    private BluetoothSocket getSocket(BluetoothDevice device) throws InvocationTargetException, IllegalAccessException, InterruptedException, IOException, NoSuchMethodException {
        connectTries++;
        Log.i("BTTEST", "getSocket try: " + String.valueOf(connectTries));
        Method m = null;
        try {
            m = device.getClass().getMethod("createRfcommSocket", new Class[]{int.class});
        } catch (NoSuchMethodException e) {
            Log.i("BTTEST", "createRfcommSocket exception: "+e.getMessage());
            e.printStackTrace();
            throw e;
        }
        BluetoothSocket socket = null;
//                    Log.i("BTTEST", "createRfcommSocket");
        socket = (BluetoothSocket) m.invoke(device, 1);
//        socket = device.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805f9b34fb"));
        try {
            socket.connect();
            Log.i("BTTEST", "socket.connected()");
        } catch (Exception e) {
            Log.i("BTTEST", "getSocket exception: " + e.getMessage());
            if (connectTries > 10) {
                throw e;
            }
            Thread.sleep(1500);
            return getSocket(device);
        }
        Log.i("BTTEST", "returning socket");
        return socket;
    }

    private static byte[] readBuffer;
    private static int readBufferPosition;
    private static Thread workerThread;

    private void beginListenForData() {
        final Handler handler = new Handler();

        // this is the ASCII code for a newline character
        final byte delimiter = 10;

        stopWorker = false;
        readBufferPosition = 0;
        readBuffer = new byte[1024];

        workerThread = new Thread(new Runnable() {
            public void run() {

                while (!Thread.currentThread().isInterrupted() && !stopWorker) {
//                    Log.i("BTTEST", "!Thread.currentThread().isInterrupted()");
                    try {

                        int bytesAvailable = inputStream.available();
//                        Log.i("BTTEST", "bytesAvailable "+String.valueOf(bytesAvailable));

                        if (bytesAvailable > 0) {

                            byte[] packetBytes = new byte[bytesAvailable];
                            inputStream.read(packetBytes);

                            for (int i = 0; i < bytesAvailable; i++) {

                                byte b = packetBytes[i];
                                if (b == delimiter) {

                                    byte[] encodedBytes = new byte[readBufferPosition];
                                    System.arraycopy(
                                            readBuffer, 0,
                                            encodedBytes, 0,
                                            encodedBytes.length
                                    );

                                    // specify US-ASCII encoding
                                    final String data = new String(encodedBytes, "US-ASCII");
                                    readBufferPosition = 0;

                                    // tell the user data were sent to bluetooth printer device
                                    handler.post(new Runnable() {
                                        public void run() {
                                            Log.i("BTTEST", "beginListenForData "+data);
                                        }
                                    });

                                } else {
                                    readBuffer[readBufferPosition++] = b;
                                }
                            }
                        }

                    } catch (IOException ex) {
                        stopWorker = true;
                    }

                }
            }
        });

        workerThread.start();
    }

}
